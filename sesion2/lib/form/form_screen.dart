import 'package:flutter/material.dart';
import 'package:sesion_2/form/text_field_widget.dart';

class FormScreem extends StatefulWidget {
  const FormScreem({Key? key}) : super(key: key);

  @override
  State<FormScreem> createState() => _FormScreemState();
}

class _FormScreemState extends State<FormScreem> {

  final keyform = GlobalKey<FormState>();
  TextEditingController textCodeBoardController = TextEditingController();
  TextEditingController textTempBoardController = TextEditingController();
  TextEditingController textObservationController = TextEditingController();


  String? codeBoard;
  String? tempBoard;
  String? observation;
  int question1 = 1;
  int question2 = 1;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          child: Form(
            key: keyform,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                  const Center(
                    child: Text(
                      'Table 01',
                      style: TextStyle(
                        fontSize: 25,
                      )
                    ),
                  ),
                  const SizedBox(height: 16.0),
                  TextFieldWidget(
                    onSaved: (val) => codeBoard = val,
                    validator: _validateRequired,
                    textTitle: "Código de tablero",
                    textHint: "Ejm: AT01",
                    textController: textCodeBoardController,
                  ),
                  const SizedBox(height: 16.0),
                  TextFieldWidget(
                    onSaved: (val) => tempBoard = val,
                    validator: _validateRequired,
                    keyboardType: TextInputType.text ,
                    textTitle: "Temperatura del tablero",
                    textHint: "Ejm: 40",
                    textController: textTempBoardController,
                  ),
                  const SizedBox(height: 16.0),
                  TextFieldWidget(
                    onSaved: (val) => observation = val ,
                    validator: _validateRequired,
                    textTitle: "Observaciones",
                    textHint: "",
                    textController: textObservationController,
                    maxLines: 5,
                  ),
                  ElevatedButton(
                    onPressed: () {
                      keyform.currentState?.save();
                      bool validarKeyForm = keyform.currentState!.validate();
                      if (validarKeyForm){
                        print(codeBoard );
                        print(tempBoard);
                        print(observation);
                        Navigator.pop(context);
                      }
                    },
                    child: const Text( "Guardar" ),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  String? _validateRequired(String? value) {
    if (value == null || value.isEmpty) return 'El dato es requerido';
    return null;
  }

}