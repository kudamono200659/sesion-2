class UserModel {
  int id;
  int email;
  int firtsName;
  int lastName;
  int avatar;
  
  UserModel({
    required this.id,
    required this.email,
    required this.firtsName,
    required this.lastName,
    required this.avatar,
  });

  Map<String, dynamic> get toMap => {
    'codeBoard' : id,
    'tempBoard' : email,
    'question1' : firtsName,
    'question2' : lastName,
    'question3' : avatar
  };
}