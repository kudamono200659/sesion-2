import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;



class HttpScreen extends StatefulWidget {
  const  HttpScreen({Key? key}) : super(key: key);

  @override
  State<HttpScreen> createState() => _HttpScreenState();
}

class _HttpScreenState extends State<HttpScreen> {

    var url = Uri.https('reqres.in','api/users?page=2');

    @override
    void initState() {
      super.initState();
      WidgetsBinding.instance.addPostFrameCallback((_) async{
          var response = await http.get(url);
          if (response.statusCode == 200) {
            var jsonResponse = convert.jsonDecode(response.body) as Map<String, dynamic>;
            print('===== jsonResponse ====== ');
            print(jsonResponse);
            print('===== data ====== ');
            print(jsonResponse['data']);
          } else {
            print('Request failed with status: ${response.statusCode}.');
          }
      });
    }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Primer pantalla')),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text('retornar')
            ),
          ],
        ),
      )
    );
  }
}