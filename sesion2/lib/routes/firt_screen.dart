import 'package:flutter/material.dart';
import 'package:sesion_2/form/form_screen.dart';
import 'package:sesion_2/http/http_screen.dart';
import 'package:sesion_2/routes/secont_screen.dart';
import 'package:sesion_2/shared_preference/shared_preference_screen.dart';

class FirtsScreem extends StatefulWidget {
  const FirtsScreem({Key? key}) : super(key: key);

  @override
  State<FirtsScreem> createState() => _FirtsScreemState();
}

class _FirtsScreemState extends State<FirtsScreem> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Primer pantalla')),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => const SecontScreen() ));
              },
              child: const Text('Ir a la pantalla 2')
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => const SharedPreferenceScreen() ));
              },
              child: const Text('Ejemplo del sharepreferences')
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => const HttpScreen() ));
              },
              child: const Text('Consumo de http')
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => const FormScreem() ));
              },
              child: const Text('Formularios')
            ),
          ],
        ),
      )
    );
  }
}
//Emulador
//Explicacion de widget.
//Usamos el navigator
//Pasa argumentos entre las pantallas.
//Pasa una Funcion() como argumento
//Crear diferentes botones
//Convertir un stateless a statefull


//https://reqres.in/api/users?page=2