import 'package:flutter/material.dart';
import 'package:sesion_2/routes/four_sreen.dart';

class TreeScreen extends StatefulWidget {
  final String title;
  const TreeScreen({Key? key, required this.title}) : super(key: key);

  @override
  State<TreeScreen> createState() => _TreeScreenState();
}

class _TreeScreenState extends State<TreeScreen> {
  int cont = 0;

  void incrementCounter(String? data) {
      cont++;
      print(data);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        backgroundColor: Colors.green,

      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(cont.toString(), style: const TextStyle(fontSize: 30),),
            const SizedBox(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text('Retornar')
                ),
                const SizedBox(width: 10),
                ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => FourScreem(onSum: incrementCounter,)));
                  },
                  child: const Text('ir a la pantalla 4')
                ),
              ],
            ),
          ],
        ),
      )
    );
  }
}