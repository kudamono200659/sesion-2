import 'package:flutter/material.dart';

class FourScreem extends StatefulWidget {
  final Function(String? data)? onSum;
  const FourScreem({Key? key, this.onSum}) : super(key: key);

  @override
  State<FourScreem> createState() => _FourScreemState();
}

class _FourScreemState extends State<FourScreem> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Pantalla 4'),
        backgroundColor: Colors.green,

      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GestureDetector(

              onTap: () {
                widget.onSum!('soy parametro');
                Navigator.of(context).pop();

              },
              child: Container(
                padding: const EdgeInsets.all(8),
                decoration: BoxDecoration(
                  color: Colors.red,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: const Text('Sumar', style: TextStyle(color: Colors.white),)
              ),
            ),
            const SizedBox(height: 15),
            InkWell(
              borderRadius: BorderRadius.circular(10),
              onTap: () {
                  Navigator.of(context).pop();
              },

              child: Container(
                padding: const EdgeInsets.all(8),
                decoration: BoxDecoration(
                  color: Colors.amber,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: const Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                child: const Text('Retornar')
              ),
            ),
          ],
        ),
      )
    );
  }
}