import 'package:flutter/material.dart';
import 'package:sesion_2/routes/firt_screen.dart';
import 'package:sesion_2/routes/navigation/routes.dart';

class SecontScreen extends StatefulWidget {
  const SecontScreen({Key? key}) : super(key: key);

  @override
  State<SecontScreen> createState() => _SecontScreenState();
}

class _SecontScreenState extends State<SecontScreen> {
  @override
  Widget build(BuildContext context) {
      return Scaffold(
      appBar: AppBar(
        title: const Text('Segunda pantalla'),
        backgroundColor: Colors.red,

      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => const FirtsScreem() ));
              },
              child: const Text('ir a la pantalla 1')
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text('Retornar')
            ),
            ElevatedButton(
              onPressed: () => Navigator.of(context).push(Routes.totreeScreenPage(title: 'Pantalla 3')),
              child: const Text('ir a la pantalla 3')
            ),
          ],
        ),
      )
    );
  }
}