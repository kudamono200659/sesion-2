import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';

class AnimacionesScreen extends StatelessWidget {
  const AnimacionesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('animaciones'),),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            BounceInDown(
              child:ElevatedButton(
                onPressed: () {
                }, child: const Text('hola')
              )
            ),
            FadeInUp(
              child: Container(
                width: 100,
                height: 40,
                color: Colors.amber,
              ),
            ),
            FadeInDown(
              child: Container(
                width: 100,
                height: 40,
                color: Colors.amber,
              ),
            ),
            FadeInRight(
              child: Container(
                width: 100,
                height: 40,
                color: Colors.amber,
              ),
            ),
          ],
        ),
      )
    );
  }
}