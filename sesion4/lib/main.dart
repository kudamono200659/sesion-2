import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:sesion4/acces/acces_screen.dart';
import 'package:sesion4/animaciones/animaciones_screen.dart';
import 'package:sesion4/firebase/firebase_screem.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  Firebase.initializeApp().then((_) {
    runApp(const MyApp());
  });
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final int _selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("List temas"),
      ),
      body:  Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => const FirebaseScreem())),
              child: const Text('Firebase')
            ),
            ElevatedButton(
              onPressed: () => Navigator.of(context).push( MaterialPageRoute(builder: (context) => const AnimacionesScreen())),
              child: const Text('Animaciones')
            ),
            ElevatedButton(
              onPressed: () => Navigator.of(context).push( MaterialPageRoute(builder: (context) => const AccesScreen())),
              child: const Text('acces')
            )
          ],
        ),
      ),
    );
  }
}
