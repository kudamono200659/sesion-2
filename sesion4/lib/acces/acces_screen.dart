

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';

class AccesScreen extends StatefulWidget {
  const AccesScreen({Key? key}) : super(key: key);

  @override
  State<AccesScreen> createState() => _AccesScreenState();
}

class _AccesScreenState extends State<AccesScreen> {


  File? imageFile;
  XFile? image;
  // final XFile? images;
  final picker = ImagePicker();
  final ImagePicker _picker = ImagePicker();

  @override
  void initState() {
    super.initState();
    locator();
  }

  locator()async{
    LocationPermission permission = await Geolocator.requestPermission();
    Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    print(position);
    // bool permission = await Geolocator.isLocationServiceEnabled() ;
    // print(permission);

  }

  opciones(context){
    showDialog(
      context: context,
      builder: (BuildContext context){
        return AlertDialog(
          contentPadding: const EdgeInsets.all(0),
          content: SingleChildScrollView(
            child: Column(
              children: [
                InkWell(
                  onTap: () async{
                    var picture = await picker.getImage(source: ImageSource.camera);
                    imageFile = File(picture!.path);
                  },
                  child: Container(
                    padding: const EdgeInsets.all(20),
                    decoration: const BoxDecoration(
                      border: Border(bottom: BorderSide(width: 1, color:  Colors.grey ))
                    ),
                    child: Row(
                      children: const [
                        Expanded(
                          child: Text('Tomar una foto', style: TextStyle(fontSize: 16),)
                        ),
                        Icon(Icons.camera_alt, color: Colors.amber )
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () async {
                    final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
                  },
                  child: Container(
                    padding: const EdgeInsets.all(20),
                    child: Row(
                      children: const [
                        Expanded(
                          child: Text('Seleccionar una foto', style: TextStyle(fontSize: 16),)
                        ),
                        Icon(Icons.image , color: Colors.blue )
                      ],
                    ),
                  ),
                )
              ],
            ),
          )
        );
      }
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Selecciona una Imagen')),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                ElevatedButton(
                  onPressed: () {
                    opciones(context);
                  },
                child: const Text('Selecciona una imagen')
                ),
                const SizedBox(height: 30,),
                // image == null ? const Center(): Image.file(image!)
              ],
            ),
          )
        ],
      )
    );
  }
}