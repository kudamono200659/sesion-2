import 'package:flutter/material.dart';

class TextFieldWidget extends StatefulWidget {
  final String textTitle;
  final String textHint;
  final int maxLines;
  final bool enabled;
  final TextEditingController textController;
  final TextInputType keyboardType;
  final onSaved;
  final validator;

  const TextFieldWidget(
      {Key? key,
      required this.textTitle,
      required this.textHint,
      required this.textController,
      this.enabled = true,
      this.keyboardType = TextInputType.text,
      this.onSaved,
      this.validator,
      this.maxLines = 1})
      : super(key: key);

  @override
  State<TextFieldWidget> createState() => _TextFieldWidgetState();
}

class _TextFieldWidgetState extends State<TextFieldWidget> {
  bool selectedField = false;
  String text = "";

  @override
  void dispose() {
    //widget.textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    text = widget.textController.text;
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          color: selectedField ? Colors.black : Colors.black ,
          width: 1.0,
        ),
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: Focus(
        onFocusChange: (hasFocus) {
          setState(() {
            selectedField = hasFocus;
          });
        },
        child: TextFormField(
          onSaved: widget.onSaved,
          validator: widget.validator,
          controller: widget.textController,
          maxLines: widget.maxLines,
          enabled: widget.enabled,
          keyboardType: widget.keyboardType,
          decoration: InputDecoration(
              border: InputBorder.none,
              focusedBorder: InputBorder.none,
              enabledBorder: InputBorder.none,
              errorBorder: InputBorder.none,
              disabledBorder: InputBorder.none,
              labelText: widget.textTitle,
              labelStyle: const TextStyle(
                color: Colors.black,
                fontSize: 14.0,
              ),
              floatingLabelStyle: const TextStyle(
                color: Colors.black,
              ),
              contentPadding:
                  const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
              hintText: widget.textHint),
        ),
      ),
    );
  }
}
