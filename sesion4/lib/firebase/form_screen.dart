
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:sesion4/firebase/module/text_field_widget.dart';

class FormScreem extends StatefulWidget {
  final id;
  final data;
  const FormScreem({Key? key, this.data,this.id}) : super(key: key);

  @override
  State<FormScreem> createState() => _FormScreemState();
}

class _FormScreemState extends State<FormScreem> {

  FirebaseFirestore db = FirebaseFirestore.instance;
  final keyform = GlobalKey<FormState>();
  TextEditingController textCodeBoardController = TextEditingController();
  TextEditingController textTempBoardController = TextEditingController();


  String? codeBoard;
  String? tempBoard;
  String? observation;
  @override
  void initState() {
    super.initState();
    if (widget.id != null) {
      textCodeBoardController.text = widget.data['name'];
      textTempBoardController.text = widget.data['edad'];
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title:const Text('Form')),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          child: Form(
            key: keyform,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                  const SizedBox(height: 16.0),
                  TextFieldWidget(
                    onSaved: (val) => codeBoard = val,
                    validator: _validateRequired,
                    textTitle: "nombre",
                    textHint: "Ejm: julio",
                    textController: textCodeBoardController,
                  ),
                  const SizedBox(height: 16.0),
                  TextFieldWidget(
                    onSaved: (val) => tempBoard = val,
                    validator: _validateRequired,
                    keyboardType: TextInputType.text ,
                    textTitle: "Edad",
                    textHint: "Ejm: 40",
                    textController: textTempBoardController,
                  ),
                  const SizedBox(height: 16.0),
                  ElevatedButton(
                    onPressed: () async {
                      keyform.currentState?.save();
                      bool validarKeyForm = keyform.currentState!.validate();
                      if (validarKeyForm){
                        if (widget.id == null) {
                          await db.collection('user').add(
                          {
                            'name': codeBoard,
                            'edad': tempBoard
                          });
                        } else {
                          await db.collection('user').doc(widget.id).update(
                            {
                              'name': codeBoard,
                              'edad': tempBoard
                            }
                          );
                        }
                        Navigator.pop(context);
                      }
                    },
                    child: const Text( "Guardar" ),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  String? _validateRequired(String? value) {
    if (value == null || value.isEmpty) return 'El dato es requerido';
    return null;
  }
}
