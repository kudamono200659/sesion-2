
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:sesion4/firebase/form_screen.dart';


class FirebaseScreem extends StatefulWidget {
  const FirebaseScreem({Key? key}) : super(key: key);

  @override
  State<FirebaseScreem> createState() => _FirebaseScreemState();
}

class _FirebaseScreemState extends State<FirebaseScreem> {

  FirebaseFirestore db = FirebaseFirestore.instance;

  CollectionReference collectionReference = FirebaseFirestore.instance.collection('user');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Firebase"),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => const FormScreem()));
            },
            icon: const Icon(Icons.add),
          )
        ],
      ),
      body: StreamBuilder(
        stream: collectionReference.snapshots(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return const Text("error");
          }
          if (snapshot.connectionState == ConnectionState.done) {
            return const Text('loading');
          }
          if(snapshot.hasData ){
            return ListView.builder(
              itemCount: snapshot.data!.docs.length,
              itemBuilder: (BuildContext context, int index) {
                Object? ds = snapshot.data?.docs[index].data();
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.circular(8)
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Text("$ds", style: const TextStyle(fontSize: 30,color: Colors.white),),
                          const Divider(),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              IconButton(
                                onPressed: () {
                                  String? id = snapshot.data?.docs[index].id;
                                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => FormScreem(id: id ,data: snapshot.data?.docs[index].data(),)));
                                },
                                icon: const Icon(Icons.edit)
                              ),
                              IconButton(
                                onPressed: () async {
                                  await db.collection('user').doc(snapshot.data?.docs[index].id).delete();
                                },
                                icon: const Icon(Icons.delete)
                              )
                            ],
                          )
                        ],
                      ),
                    )),
                );
              },
            );
          }
          return Container();
        },
      ),
    );
  }
}